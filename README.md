![Logo Compasso](https://compasso.com.br/wp-content/uploads/2020/07/LogoCompasso-Negativo.png)
# Implementação do Micro Serviço Catálogo de Produtos

Implementação do micro serviço 'Catálogo de Produtos', conforme especificação dos requisitos [disponíveis aqui.](https://bitbucket.org/RecrutamentoDesafios/desafio-java-springboot/)

Basicamente este micro serviço fornece um `CRUD` básico, incluindo um serviço de busca com filtros para um Catálogo de Produtos.


## Como este micro serviço foi implementado? ##

Em resumo, a implementação do micro serviço foi realizada utilizando a linguagem `JAVA 11`, sobre o Framework `Spring Boot`.

**Considerações importantes**

- Toda a implementação do desafio, seguiu as boas práticas de desenvolvimento e de padrões de projeto, para elaboração de um código limpo, conciso e de fácil entendimento;
- Por se tratar de um projeto novo, foram empregadas as versões de `dependências estáveis mais recentes`;
- A camada de persistência foi implementada utilizando uma arquitetura moderna e `escalável` entregando `alta disponibilidade`. Para isso foi utilizado um banco de dados NoSQL orientado a documentos `MongoDB`;
- Durante o desenvolvimento, foi observado o correto versionamento do código com o GIT, seguindo um GIT FLOW básico;
- Foi disponibilizada uma documentação da API REST, utilizando o `Swagger2`.
- Foi realizada a implementação de `testes unitários`.



## **Como Executar este Micro Serviço?** ##

- O ambiente de execução que inclui toda a infra-estrutura e dependências necessárias para executar corretamente o micro-serviço foi implementado utilizando containeres `docker`.

- Para melhor organização do projeto, o versionamento do `código fonte` da aplicação e do `docker` foram configurados em repositórios diferentes.

- **IMPORTANTE:** O repositório do ambiente `docker`, que inclui toda a `documentação necessária` para configuração do ambiente está [disponível aqui](https://bitbucket.org/murilo8812/docker_ms_product/src/master/).
  * A documentação possui uma seção específica que detalha como acessar os serviços disponibilizados pelos containeres, como por exempo acesso a documentação da API com `swagger` e ferramenta de gerenciamento do `mongoDB`

- ***Observação***: O repositório `docker` inclui o `build` mais recente do micro serviço. Apesar de existirem ferramentas especificas de `CI/CD` que automatizam o build e deploy, a inclusão do artefato `.jar` do build foi feita com o objetivo de facilitar o deploy durante a construção da imagem do container.

## **Repositório Docker** ##

https://bitbucket.org/murilo8812/docker_ms_product