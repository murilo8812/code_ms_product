package com.compassouol.challenges.productms;

import com.compassouol.challenges.productms.controller.ProductController;
import com.compassouol.challenges.productms.dto.ProductCriteria;
import com.compassouol.challenges.productms.model.document.Product;
import com.compassouol.challenges.productms.service.ProductService;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;

@WebMvcTest
@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {

    @Autowired
    private ProductController productController;

    @MockBean
    private ProductService productService;

    @BeforeEach
    public void setup() {
        standaloneSetup(this.productController);
    }

    @Test
    void deveRetornarCreated_QuandoCriarNovoProduto() {
        final Product p = new Product();
        p.setName("Notebook Dell Vostro");
        p.setDescription("1TB 16GB RAM");
        p.setPrice(new BigDecimal("1500.00"));

        Mockito.when(this.productService.create(p))
                .thenReturn(p);

        given()
                .contentType(ContentType.JSON)
                .body(p)
                .when()
                    .post("/products")
                .then()
                    .statusCode(HttpStatus.CREATED.value());
    }

    @Test
    void deveRetornarSucesso_QuandoBuscarProduto() {
        final Optional<Product> p = Optional.of(
                new Product("abcde", "Notebook Dell Vostro", "16GB", new BigDecimal("1500.00"))
        );
        Mockito.when(this.productService.getById("abcde"))
                .thenReturn(p);
        given()
                .accept(ContentType.JSON)
                .when()
                    .get("/products/{id}", "abcde")
                .then()
                    .statusCode(HttpStatus.OK.value());
    }

    @Test
    void deveRetornarBadRequest_QuandoAtualizaProdutoSemNome(){
        String id = "abc";
        Product p = new Product(id, "", "descrição teste", BigDecimal.valueOf(122.04));

        given()
                .contentType(ContentType.JSON)
                .body(p)
                .when()
                    .put("/products/{id}", id)
                .then()
                    .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    void deveRetornarBadRequest_QuandoAtualizaProdutoValorNegativo(){
        String id = "abc";
        Product p = new Product(id, "", "descrição teste", BigDecimal.valueOf(-122.04));

        given()
                .contentType(ContentType.JSON)
                .body(p)
                .when()
                    .put("/products/{id}", id)
                .then()
                    .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    void deveRetornarSucesso_QuandoBuscaTodosProdutos(){
        Product product = new Product();
        List<Product> products = new ArrayList<>();
        products.add(product);

        Mockito.when(this.productService.getAll())
                .thenReturn(products);

        given()
                .accept(ContentType.JSON)
                .when()
                    .get("/products")
                .then()
                    .statusCode(HttpStatus.OK.value());
    }

    @Test
    void deveRetornarSucesso_QuandoRemoveProduto() {
        Mockito.when(this.productService.delete("abcde"))
                .thenReturn(Optional.of("abcde"));

        given()
                .accept(ContentType.JSON)
                .when()
                    .delete("/products/{id}", "abcde")
                .then()
                    .statusCode(HttpStatus.OK.value());
    }

    @Test
    void deveRetornarSucesso_QuandoBuscaProdutos(){
        Product p = new Product();
        List<Product> products = Collections.singletonList(p);
        final ProductCriteria criteria = new ProductCriteria("dell", BigDecimal.valueOf(1500.00), BigDecimal.valueOf(2500.00));

        Mockito.when(this.productService.filter(criteria))
                .thenReturn(products);

        given()
                .accept(ContentType.JSON)
                .when()
                    .get("/products/search?q=dell&min_price=1500.00&max_price=2500.00")
                .then()
                    .statusCode(HttpStatus.OK.value());
    }

}
