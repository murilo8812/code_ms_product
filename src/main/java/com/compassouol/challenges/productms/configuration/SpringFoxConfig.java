package com.compassouol.challenges.productms.configuration;

import com.compassouol.challenges.productms.dto.StatusInfoResponse;
import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SpringFoxConfig {

    @Value("${app.metadata.name}")
    private String appName;

    @Value("${app.metadata.description}")
    private String appDescription;

    @Value("${app.metadata.build.version}")
    private String appVersion;

    @Value("${app.metadata.maintainer.name}")
    private String appMaintainer;

    @Value("${app.metadata.maintainer.mail}")
    private String appMaintainerMail;


    @Bean
    public Docket api(TypeResolver typeResolver) {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.compassouol.challenges.productms.controller"))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, buildDefaultResponseMessagesErrors(RequestMethod.GET))
                .globalResponseMessage(RequestMethod.POST, buildDefaultResponseMessagesErrors(RequestMethod.POST))
                .globalResponseMessage(RequestMethod.PUT, buildDefaultResponseMessagesErrors(RequestMethod.PUT))
                .globalResponseMessage(RequestMethod.DELETE, buildDefaultResponseMessagesErrors(RequestMethod.DELETE))
                .apiInfo(this.metaData())
                .additionalModels(typeResolver.resolve(StatusInfoResponse.class));
    }

    private List<ResponseMessage> buildDefaultResponseMessagesErrors(RequestMethod requestMethod) {
        final ArrayList<ResponseMessage> messages = new ArrayList<>();
        Map<RequestMethod, String> actionMap = new HashMap<>();
        actionMap.put(RequestMethod.GET, "obter");
        actionMap.put(RequestMethod.POST, "criar");
        actionMap.put(RequestMethod.PUT, "atualizar");
        actionMap.put(RequestMethod.PATCH, "atualizar");
        actionMap.put(RequestMethod.DELETE, "remover");

        messages.add(new ResponseMessageBuilder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(String.format("Falha no servidor ao tentar %s o recurso.", actionMap.get(requestMethod)))
                .build()
        );

        if (requestMethod.equals(RequestMethod.PUT) || requestMethod.equals(RequestMethod.DELETE))
        messages.add(new ResponseMessageBuilder()
                .code(HttpStatus.NOT_FOUND.value())
                .message(String.format("Ao tentar %s um recurso que não existe.", actionMap.get(requestMethod)))
                .build()
        );

        if (requestMethod.equals(RequestMethod.POST) || requestMethod.equals(RequestMethod.PUT))
        messages.add(new ResponseMessageBuilder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(String.format("Ao tentar %s um recurso com informações inválidas, ou ausentes.", actionMap.get(requestMethod)))
                .responseModel(new ModelRef("StatusInfoResponse"))
                .build()
        );

        return messages;
    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title(this.appName)
                .description(this.appDescription)
                .version(this.appVersion)
                .contact(new Contact(this.appMaintainer,"", this.appMaintainerMail))
                .build();
    }
}
