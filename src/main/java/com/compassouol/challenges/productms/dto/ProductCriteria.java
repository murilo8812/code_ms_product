package com.compassouol.challenges.productms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Value;

import java.math.BigDecimal;

/**
 * DTO imutável para mapeamento de filtros de Products
 */

@ApiModel(description = "Especifica vários tipos de filtro para a busca de produtos")
@Value
public class ProductCriteria {

    @ApiParam("Valor a ser procurado nos campos name OU description. " +
            "A busca é case insensitive, ou seja, não diferencia letras maiúsculas ou minúsculas.")
    String q;

    BigDecimal min_price;

    BigDecimal max_price;
}
