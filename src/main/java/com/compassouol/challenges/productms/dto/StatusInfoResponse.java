package com.compassouol.challenges.productms.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * DTO auxiliar para retorno do código status http e uma mensagem genérica
 *
 * @param <T> Tipo de mensagem
 */

@Data
@Builder
public class StatusInfoResponse<T> {
    @Builder.Default
    @JsonIgnore
    private HttpStatus httpStatus = HttpStatus.OK;
    private T message;

    /**
     *
     * @return int valor do status  https
     */
    public Integer getStatusCode() {
        return this.httpStatus.value();
    }

}
