package com.compassouol.challenges.productms.service;

import com.compassouol.challenges.productms.dto.ProductCriteria;
import com.compassouol.challenges.productms.model.document.Product;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Product create(Product p);

    Optional<Product> update(Product p);

    Optional<Product> getById(String id);

    List<Product> getAll();

    List<Product> filter(@NonNull ProductCriteria criteria);

    Optional<String> delete(String id);
}
