package com.compassouol.challenges.productms.service;

import com.compassouol.challenges.productms.dto.ProductCriteria;
import com.compassouol.challenges.productms.model.document.Product;
import com.compassouol.challenges.productms.model.document.QProduct;
import com.compassouol.challenges.productms.repository.ProductRepository;
import com.querydsl.core.BooleanBuilder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl extends BaseService<ProductRepository> implements ProductService {

    /**
     * Cria um novo produto na camada de persistência
     *
     * @param p Dados do produto
     * @return Dados do produto, incluindo o ID gerado.
     */
    public Product create(Product p) {
        return super.getRepository().save(p);
    }

    /**
     * Atualiza o estado de um produto
     *
     * @param p Dados do produto a ser atualizado, com um ID válido.
     * @return Produto com os dados atualizados, ou null caso não existir produto com o ID informado
     */
    public Optional<Product> update(Product p) {
        if (! super.getRepository().existsById(p.getId())) {
            return Optional.empty();
        }
        return Optional.of(super.getRepository().save(p));
    }

    /**
     * Obtém um produto pelo ID
     *
     * @param id a ser pesquisado
     * @return Dados do produto, ou null caso não existir produto com o ID informado
     */
    public Optional<Product> getById(String id) {
        return super.getRepository().findById(id);
    }

    /**
     *
     * @return Todos os produtos cadastrados na camada de persistência.
     */
    public List<Product> getAll() {
        return super.getRepository().findAll();
    }

    /**
     * Obtém produtos da camada de persistência de acordo com os critérios especificados no filtro
     *
     * @param criteria Critérios de filtro (todos opcionais)
     * @return Produtos que correspondem aos parâmetros do filtro.
     *
     */
    public List<Product> filter(@NonNull ProductCriteria criteria) {
        final QProduct qProd = QProduct.product;
        final BooleanBuilder builder = new BooleanBuilder();

        // price >= min_price
        Optional.ofNullable(criteria.getMin_price())
                .ifPresent(min -> builder.and(qProd.price.goe(min)));
        // price <= max_price
        Optional.ofNullable(criteria.getMax_price())
                .ifPresent(max -> builder.and(qProd.price.loe(max)));
        // name - description like `%q%`
        Optional.ofNullable(criteria.getQ())
                .filter(q -> ! q.isBlank())
                .map(String::trim)
                .ifPresent(q -> builder.and(qProd.name.containsIgnoreCase(q).or(qProd.description.containsIgnoreCase(q))));

        return builder.hasValue() ? (List<Product>) super.getRepository().findAll(builder.getValue()) :
                super.getRepository().findAll();
    }

    /**
     * Remove um produto da camada de persistência.
     *
     * @param id id a ser removido
     * @return o mesmo id especificado na chamada, senão null caso não existir nenhum produto com o id especificado.
     */
    public Optional<String> delete(String id) {
        if (! super.getRepository().existsById(id)) {
            return Optional.empty();
        }
        super.getRepository().deleteById(id);
        return Optional.of(id);
    }

}
