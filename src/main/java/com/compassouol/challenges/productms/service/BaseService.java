package com.compassouol.challenges.productms.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Classe base para implementação de bean services que obrigatoriamente
 * utilizam pelo menos 1 repositório de acesso a dados
 *
 * @param <T> ManagedBean do tipo @Repository padrão para o serviço
 */

public abstract class BaseService<T> {

    /**
     * Repositório padrão para o service bean
     */
    @Getter
    @Autowired
    private T repository;
}
