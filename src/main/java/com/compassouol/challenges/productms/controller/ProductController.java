package com.compassouol.challenges.productms.controller;

import com.compassouol.challenges.productms.dto.ProductCriteria;
import com.compassouol.challenges.productms.exception.ResourceNotFoundException;
import com.compassouol.challenges.productms.model.document.Product;
import com.compassouol.challenges.productms.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Controlador de 'Products' resources
 */

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService service;

    @ApiOperation("Cria um novo produto")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Os dados do produto foram cadastrados com sucesso."),
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Product> create(
            @Valid @RequestBody Product p
    ) {
        return new ResponseEntity<>(this.service.create(p), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Altera os dados de um produto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Os dados do produto foram atualizados com sucesso.")
    })
    @PutMapping(path = "{id}")
    public ResponseEntity<Product> update(
            @PathVariable String id,
            @Valid @RequestBody Product p
    ) {
        p.setId(id);
        p = this.service.update(p)
                .orElseThrow(() -> new ResourceNotFoundException(id, Product.class));
        return ResponseEntity.ok(p);
    }

    @ApiOperation(value = "Obtém os dados de um produto específico, através do ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Dados do produto foram obtidos com sucesso"),
            @ApiResponse(code = 404, message = "O recurso solicitado com o ID especificado não existe")
    })
    @GetMapping(path = "{id}")
    public ResponseEntity<Product> getById(
            @PathVariable String id
    ) {
        Product p = this.service.getById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, Product.class));
        return ResponseEntity.ok(p);
    }

    @ApiOperation(value = "Obtém os dados de todos os produtos cadastrados.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Produtos foram obtidos com sucesso")
    })
    @GetMapping
    public ResponseEntity<List<Product>> getAll() {
        return ResponseEntity.ok(this.service.getAll());
    }

    @ApiOperation(value = "Remove um produto cadastrado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Produto removido com sucesso.")
    })
    @DeleteMapping(path = "{id}")
    public ResponseEntity<Void> delete(
            @PathVariable String id
    ) {
         this.service.delete(id)
                 .orElseThrow(() -> new ResourceNotFoundException(id, Product.class));
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Obtém produtos cadastrados, com base nos critérios de busca especificados")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Busca executada com sucesso, com ou sem resultados.")
    })
    @GetMapping(path = "/search")
    public ResponseEntity<List<Product>> filter(
            ProductCriteria filterDto
    ) {
        return ResponseEntity.ok(this.service.filter(filterDto));
    }

}
