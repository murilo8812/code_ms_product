package com.compassouol.challenges.productms.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ResourceNotFoundException extends RuntimeException {

    private String id;

    public <T> ResourceNotFoundException(String id, Class<T> entityClass) {
        super(String.format("Entidade do tipo %s com ID `%s` não existe.", entityClass.getName(), id));
        this.id = id;
    }
}
