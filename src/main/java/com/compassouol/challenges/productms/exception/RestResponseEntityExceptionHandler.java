package com.compassouol.challenges.productms.exception;

import com.compassouol.challenges.productms.dto.StatusInfoResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handler para tratamento de erros de validação (Bean Validation), para customização do response body.
     *
     * @param ex Dados da exception
     * @param headers headers do response
     * @param status status do response
     * @param request dados da requisição
     * @return Response entity com os dados do erro
     * @see ResponseEntityExceptionHandler
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        /* TODO sugestão de melhoria do desafio: no campo message, retornar uma lista, com todos os erros de validação.
        final List<String> errMessages = ex.getBindingResult().getFieldErrors()
                .stream()
                .map(e -> String.format("%s -> %s [%s]", e.getField(), e.getDefaultMessage(), e.getRejectedValue()))
                .collect(Collectors.toList());
        */

        final StatusInfoResponse<String> body = StatusInfoResponse.<String>builder()
                .httpStatus(status)
                .message("VALIDATION_ERRORS") // mensagem genérica, conforme especificação do item
                .build();

        return super.handleExceptionInternal(ex, body, headers, status, request);
    }

    /**
     * Handler para tratamento de recursos ausentes
     *
     * @return Apenas o status http
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    protected ResponseEntity<?> handleResourceNotFound() {
            return ResponseEntity.notFound().build();
    }
}
