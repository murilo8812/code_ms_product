package com.compassouol.challenges.productms.model.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@ApiModel(description = "Recurso que representa os dados de um Produto.")
@Document(collection = "products")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(notes = "ID do Produto", position = 1)
    private String id;
    @ApiModelProperty(notes = "Nome do Produto", required = true, position = 2)
    @NotBlank
    private String name;
    @ApiModelProperty(notes = "Breve descrição do produto", required = true, position = 3)
    @NotBlank
    private String description;
    @ApiModelProperty(notes = "Valor de venda do Produto", required = true, position = 4)
    @NotNull
    @Positive
    @Field(targetType = FieldType.DOUBLE)
    private BigDecimal price;
}
